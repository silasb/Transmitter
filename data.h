#define BTN_WAYS 1
#define NUM_WAYS 4 + BTN_WAYS

typedef struct {
  char *name;

  int subtrim[NUM_WAYS];
  int trim[NUM_WAYS];
  // char switches[BTN_WAYS];
  // int chan_order[NUM_WAYS];
} Model;

int Channel_data[NUM_WAYS];
/*int way_pin[NUM_WAYS] = {*/
  /*LV_STK, // throttle*/
  /*RV_STK, // aileron*/
  /*RH_STK, // elevator*/
  /*LH_STK, // rudder*/
/*};*/

enum chan_order{
  THROTTLE, // 0
  AILERON,  // 1
  ELEVATOR, // 2
  RUDDER,    // 3
  AUX1,
};

int way_pin[NUM_WAYS] = {
  [THROTTLE] = LV_STK,
  [AILERON] = RH_STK,
  [ELEVATOR] = RV_STK,
  [RUDDER] = LH_STK
};

int way_min[NUM_WAYS] = {
  [THROTTLE] = 450,
  [AILERON] = 450,
  [ELEVATOR] = 450,
  [RUDDER] = 450,
  [AUX1] = 400,
};

int way_max[NUM_WAYS] = {
  [THROTTLE] = 3540,
  [AILERON] = 3540,
  [ELEVATOR] = 3540,
  [RUDDER] = 3540,
  [AUX1] = 3540,
};