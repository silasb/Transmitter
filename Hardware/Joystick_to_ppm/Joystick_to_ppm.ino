// original from https://www.rcgroups.com/forums/showthread.php?2037080-DIY-Arduino-joystick-to-PPM

#define PPM_HI PORTB |= _BV(5);
#define PPM_LO PORTB &= ~_BV(5);

#define NB_WAY 6 // number of ways
#define LOW_LENGTH 300 // How long last (in µs) a low between 2 pulses
#define MIN_PPM_PULSE 700 // minimum pulse length in µs
#define PPM_PULSE_LENGTH 1000 // how much more µs will last the max pulse length
#define PACKET_LENGTH 21000 // How long (µs) last a full trame

// trame length is fixed ! Every trame will make PACKET_LENGTH µs !
// MUST NO BE MORE THAN 32ms !!! (timer's prescaler constraint)

#define PPM_OUTPUT 13 // OUTPUT PIN

int way_value[NB_WAY];
int way_pin[NB_WAY];
int way_min[NB_WAY];
int way_max[NB_WAY];

int i = 0;
int p = 0; // temp var for duty cycle calculation
int last_i_timer = 0; // last way's value sent through PPM signal
unsigned long int trame_elapsed_time = 0;
bool output_state = LOW;

// PPM Stream
enum chan_order{
  THROTTLE,
  AILERON,
  ELEVATOR,
  RUDDER,
  AUX1,  // (CH5)  led light, or 3 pos. rate on CX-10, H7, or inverted flight on H101
  AUX2,  // (CH6)  flip control
  AUX3,  // (CH7)  still camera (snapshot)
  AUX4,  // (CH8)  video camera
  AUX5,  // (CH9)  headless
  AUX6,  // (CH10) calibrate Y (V2x2), pitch trim (H7), RTH (Bayang, H20), 360deg flip mode (H8-3D, H22)
  AUX7,  // (CH11) calibrate X (V2x2), roll trim (H7)
  AUX8,  // (CH12) Reset / Rebind
};

#define AILERON_PIN 14 + 0
#define THROTTLE_PIN 14 + 1

void setup() {
  // ppm output :
  pinMode(PPM_OUTPUT, OUTPUT);
  digitalWrite(PPM_OUTPUT, output_state);

  way_pin[THROTTLE] = THROTTLE_PIN;
  pinMode(way_pin[THROTTLE], INPUT);
  way_value[THROTTLE] = analogRead(way_pin[THROTTLE]);
  way_min[THROTTLE] = way_value[THROTTLE];
  way_max[THROTTLE] = way_value[THROTTLE];

  way_pin[AILERON] = AILERON_PIN;
  pinMode(way_pin[AILERON], INPUT);
  way_value[AILERON] = analogRead(way_pin[AILERON]);
  way_min[AILERON] = way_value[AILERON];
  way_max[AILERON] = way_value[AILERON];

  // inits arrays
  // for(i=0;i<NB_WAY;i++)
  // {
  //   way_pin[i] = 14 + i;
  //   pinMode(way_pin[i], INPUT);
  //   way_value[i] = analogRead(way_pin[i]);
  //   way_min[i] = way_value[i];
  //   way_max[i] = way_value[i];
  // }

  // init timer
  cli();          // desactivation interruptions
  TCCR1A = 0x00;  // set timer1 registers to 0
  TCCR1B = 0x00;
  TIMSK1 = 0x00;

  OCR1A = 65535;// set to the max
    // CTC mode:
  TCCR1B |= (1 << WGM12);
    // prescaler to 8, that allow (@16mhz) 32.8ms trame
  TCCR1B |= (0 << CS10);
  TCCR1B |= (1 << CS11);
  TCCR1B |= (0 << CS12);
    // timer activation
  TIMSK1 |= (1 << OCIE1A);
  sei();

  for(i=0;i<NB_WAY;i++)
  {
    if (i != 0) {
      way_min[i] = 0;
      way_max[i] = 1023;
    }
  }
}

ISR(TIMER1_COMPA_vect)
{
  TIMSK1 &= (0 << OCIE1A);
  if(output_state)
  { // END OF A HIGH, we have to wait LOW_LENGTH ms before next pulse
    output_state = LOW;
    PPM_LO;
    // digitalWrite(PPM_OUTPUT, output_state);
    OCR1A = 2 * LOW_LENGTH; // set when next timer interruption will occur
    TIMSK1 |= (1 << OCIE1A);  // restart timer
    trame_elapsed_time += LOW_LENGTH;
  }
  else
  { // END of a LOW_LENGTH, new pulse !
    output_state = HIGH;
    PPM_HI;
    // digitalWrite(PPM_OUTPUT, output_state);
    if(last_i_timer >= NB_WAY) // last way, so wait until next packet
    {
      OCR1A = (2 * PACKET_LENGTH) - (trame_elapsed_time * 2);// set when next timer interruption will occur
      TIMSK1 |= (1 << OCIE1A); // restart timer
      last_i_timer = 0;
      trame_elapsed_time = 0;
    }
    else
    {
      OCR1A = 2 * way_value[last_i_timer];// set when next timer interruption will occur
      TIMSK1 |= (1 << OCIE1A); // restart timer
      last_i_timer ++;
      trame_elapsed_time += way_value[NB_WAY];
    }
  }
}


void loop() {

  for(i=0;i<NB_WAY;i++)
  {
     if (i == THROTTLE) {
      //  p = 0;
      p = analogRead(way_pin[i]);
    } else {
      p = 1023/2;
    }

     // auto calibration...
     if(p > way_max[i]) way_max[i] = p;
     if(p < way_min[i]) way_min[i] = p;

     // Arduino map function sucks
      way_value[i] = MIN_PPM_PULSE + PPM_PULSE_LENGTH * (float)((float)(p - way_min[i]) / (float)(way_max[i] - way_min[i]));
  }
}