# TX to PPM

Simple solution to build a TX from a couple potentiometer.

## TODO

See if Arduino is fast enough to handle quick movements, if not look at https://www.pjrc.com/teensy/td_libs_PulsePosition.html


## References

https://www.rcgroups.com/forums/showthread.php?2037080-DIY-Arduino-joystick-to-PPM
https://www.rcgroups.com/forums/showthread.php?2131278-PPM-mixer-with-Ardupilot-mode-buttons
https://forum.pjrc.com/threads/25265-Arduino-RC-library