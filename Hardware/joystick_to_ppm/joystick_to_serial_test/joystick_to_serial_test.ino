#define NB_WAY 1 // number of ways
#define LOW_LENGTH 300 // (.3ms) How long last (in µs) a low between 2 pulses
#define MIN_PPM_PULSE 700//1300 // (1.3ms) minimum pulse length in µs
#define PPM_PULSE_LENGTH 1000 // (.7ms) how much more µs will last the max pulse length
#define PACKET_LENGTH 21000 // How long (µs) last a full trame

#define NB_INITIAL_PIN 14

int way_value[NB_WAY];
int way_pin[NB_WAY];
int way_min[NB_WAY];
int way_max[NB_WAY];

int i = 0;
int p = 0; // temp var for duty cycle calculation

void setup() {
  Serial.begin(9600);

  // inits arrays
  for(i=0;i<NB_WAY;i++)
  {
    way_pin[i] = NB_INITIAL_PIN + i;
    pinMode(way_pin[i], INPUT);
    way_value[i] = analogRead(way_pin[i]);
    way_min[i] = way_value[i];
    way_max[i] = way_value[i];
  }
}


void loop() {

  for(i=0;i<NB_WAY;i++)
  {
    // Read current value of way i :
    p = analogRead(way_pin[i]);

    // auto calibration...
    if(p > way_max[i]) way_max[i] = p;
    if(p < way_min[i]) way_min[i] = p;

    // Arduino map function sucks
    way_value[i] = MIN_PPM_PULSE + PPM_PULSE_LENGTH * (float)((float)(p - way_min[i]) / (float)(way_max[i] - way_min[i]));

    Serial.print(way_pin[i]);
    Serial.print("\t");
    Serial.print(way_value[i]);
    Serial.print("\t");
    Serial.print(p);
    Serial.print("\t");
    Serial.print(way_min[i]);
    Serial.print("\t");
    Serial.println(way_max[i]);
  }
}