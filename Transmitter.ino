/*#include "Arduino.h"*/
/*#include <util/delay.h>*/
/*#include <avr/io.h>*/

#include <stdlib.h>

#define DISPLAY_UPDATE_MS 100

// Use timer 1
HardwareTimer ppmTimer(1);

#define BOARD_LED_LIGHT PA5

#define PPM_out PA8 // D6

#define RV_STK PC0
#define LV_STK PC1
#define LH_STK PC2
#define RH_STK PC3

/*#define LCD_SCL PB10 // D29*/
/*#define LCD_SDA PB11 // D30*/
#define LCD_SCL PB6 // D5
#define LCD_SDA PB7 // D9
#define LCD_ADDR 0x3c

#define MICRO_TO_MILLI 1000

/*#define font_gausshauss_22 1*/
#define font_arial_8 1

int firstSensor = 0;
bool led = false;

#include "data.h"

Model model;

#include "settings.h"
#include "display.h"

int scene = SCENE_NORMAL;

#include "ppm.h"
#include "sensors.h"

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin PB1 as an output.
  pinMode(BOARD_LED_LIGHT, OUTPUT);
  pinMode(PPM_out, OUTPUT);

  // model.trim[THROTTLE] = 0;
  // model.trim[RUDDER] = -10;
  // model.subtrim[THROTTLE] = 0;
  // model.subtrim[RUDDER] = 0;

  setupPPM();
  setupSensors();
  setupDisplay();

  Serial.begin(115200);
  delay(100);

  displayWrite(led);
}

// the loop function runs over and over again forever
void loop() {
  /*Serial.print(CYCLES_PER_MICROSECOND, DEC);*/

  unsigned long currentMillis = millis();

  if (currentMillis - display_last_updated > DISPLAY_UPDATE_MS) {
    display_last_updated = currentMillis;

    switch(scene) {
      case SCENE_NORMAL:
  //      displayWrite(led);
        scene_Normal(/* data */);
        break;

      default:
        break;
    }
  }

  if (Settings_changed) {
    // model.trim[THROTTLE] = Settings_TH_trim;
    // model.trim[AILERON] = Settings_RL_trim;
    // model.trim[ELEVATOR] = Settings_PT_trim;
    // model.trim[RUDDER] = Settings_YW_trim;

    // model.subtrim[THROTTLE] = Settings_TH_sub_trim;
    // model.subtrim[AILERON] = Settings_RL_sub_trim;
    // model.subtrim[ELEVATOR] = Settings_PT_sub_trim;
    // model.subtrim[RUDDER] = Settings_YW_sub_trim;

    Settings_changed = 0;
  }

  /*
  displayData(data)
  */

  /*gpio\\*/

  /*adc_read(kj)*/

  /*firstSensor = map(analogRead(RV_STK), 0, 4095, 0, 255);*/

  //digitalWrite(BOARD_LED_LIGHT, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(1000);              // wait for a second
  //digitalWrite(BOARD_LED_LIGHT, LOW);    // turn the LED off by making the voltage LOW
  //delay(1000);              // wait for a second


  /*int p;*/
//  for(int i = 0; i < NUM_WAYS; i++) {
    /*p = analogRead(way_pin[i]);*/
//    way_value[i] = 700 + 1000 * map(analogRead(way_pin[i]), 0, 4095, 0, 255);
//    way_value[i] = 1300 + 700 * map(analogRead(way_pin[i]), 0, 4095, 0, 255);

//    Serial.println(way_value[i]);
//  }

  // way_value[i] = 700 + 1000 * map(analogRead(way_pin[i]), 0, 4095, 0, 1024);
  // way_value[i] = 1300 + 700 * map(analogRead(way_pin[i]), 0, 4095, 0, 1024);

  int p = analogRead(way_pin[THROTTLE]);
  p += model.trim[THROTTLE];
  Channel_data[THROTTLE] = MIN_PPM_PULSE + PPM_PULSE_LENGTH * (float)((float)(p - way_min[THROTTLE]) / (float)(way_max[THROTTLE] - way_min[THROTTLE])) + model.subtrim[THROTTLE];

  p = analogRead(way_pin[AILERON]);
  p += model.trim[AILERON];
  Channel_data[AILERON] = MIN_PPM_PULSE + PPM_PULSE_LENGTH * (float)((float)(p - way_min[AILERON]) / (float)(way_max[AILERON] - way_min[AILERON])) + model.subtrim[AILERON];

  p = analogRead(way_pin[ELEVATOR]);
  p += model.trim[ELEVATOR];
  Channel_data[ELEVATOR] = MIN_PPM_PULSE + PPM_PULSE_LENGTH * (float)((float)(p - way_min[ELEVATOR]) / (float)(way_max[ELEVATOR] - way_min[ELEVATOR])) + model.subtrim[ELEVATOR];

  p = analogRead(way_pin[RUDDER]);
  p += model.trim[RUDDER];
  Channel_data[RUDDER] = MIN_PPM_PULSE + PPM_PULSE_LENGTH * (float)((float)(p - way_min[RUDDER]) / (float)(way_max[RUDDER] - way_min[RUDDER])) + model.subtrim[RUDDER];

  Channel_data[AUX1] = MIN_PPM_PULSE;

  // Channel_data[THROTTLE] = analogRead(way_pin[THROTTLE]);
  // Channel_data[AILERON]  = analogRead(way_pin[AILERON]);
  // Channel_data[ELEVATOR] = analogRead(way_pin[ELEVATOR]);
  // Channel_data[RUDDER]   = analogRead(way_pin[RUDDER]);

  // convert_channel_ppm(THROTTLE);
  // convert_channel_ppm(AILERON);
  // convert_channel_ppm(ELEVATOR);
  // convert_channel_ppm(RUDDER);

  // Serial.println(way_value[ELEVATOR]);

  if (Serial.available()) {
    Settings_Read();
  }
}

int16_t map16b( int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max)
{
//  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	long y ;
	x -= in_min ;
	y = out_max - out_min ;
	y *= x ;
	x = y / (in_max - in_min) ;
	return x  + out_min ;
}