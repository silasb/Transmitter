#define font_arial_8 1

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

long int display_last_updated = 0;

enum scenes {
  SCENE_NORMAL, // 0
};

void setupDisplay() {
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  // init done
}

void displayWrite(bool led) {
  display.clearDisplay();
  display.setTextColor(WHITE);

  display.setCursor(0,0);
  display.print("LCD: ");
  display.print(led ? "1" : "0");
  display.display();
}

void scene_Normal() {
  display.clearDisplay();
  display.setTextColor(WHITE);

  display.setCursor(0,0);

  display.print("R: ");
  display.print(Channel_data[RUDDER]);

  display.print(" T: ");
  display.print(Channel_data[THROTTLE]);

  // int rudder_pos = (map(Channel_data[RUDDER],     0, 4095, 0, 50)+5) / 2;
  int raw_pos = 40 / 2 - 2.5 + model.trim[RUDDER];
  // int throttle_pos = (map(Channel_data[THROTTLE], 0, 4095, 50, 0)+5) / 2;
  int throttle_pos = 40 / 2 - 2.5 - model.trim[THROTTLE];
  // int aileron_pos = (map(Channel_data[AILERON],   0, 4095, 50, 0)+5) / 2;
  int pitch_pos = 40 / 2 - 2.5 - model.trim[ELEVATOR];
  // int elevator_pos = (map(Channel_data[ELEVATOR], 0, 4095, 0, 50)+5) / 2;
  int roll_pos = 40 / 2 - 2.5 + model.trim[AILERON];

  // display.print(" C: ");
  // display.print(pos);

  display.print(Settings_Name);

  // x, y, width, height
  display.drawRect(2, 10, 2, 40, WHITE);
  display.fillRect(0, 10+throttle_pos, 6, 5, WHITE);

  display.drawRect(0+10, SSD1306_LCDHEIGHT - 4, 40, 2, WHITE);
  display.fillRect(raw_pos+10, SSD1306_LCDHEIGHT - 6, 6, 5, WHITE);

  display.drawRect(SSD1306_LCDWIDTH - 4, 10, 2, 40, WHITE);
  display.fillRect(SSD1306_LCDWIDTH - 6, 10+pitch_pos, 6, 5, WHITE);

  display.drawRect(SSD1306_LCDWIDTH - 40 - 10, SSD1306_LCDHEIGHT - 4, 40, 2, WHITE);
  display.fillRect(SSD1306_LCDWIDTH - 40 - 10 + roll_pos, SSD1306_LCDHEIGHT - 6, 6, 5, WHITE);

  display.display();
}

