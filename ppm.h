#define PPM_MAX_100 1800 // 100%
#define PPM_MIN_100 1000 // 100%

//Channel MIN MAX values
#define CHANNEL_MAX_100	1844	//	100%
#define CHANNEL_MIN_100	204		//	100%

// #define NB_WAY 6 // number of ways
#define LOW_LENGTH 300 // (.3ms) How long last (in µs) a low between 2 pulses
#define MIN_PPM_PULSE 900//1300 // (1.3ms) minimum pulse length in µs
#define PPM_PULSE_LENGTH 1000 // (.7ms) how much more µs will last the max pulse length
#define PACKET_LENGTH 21000 // How long (µs) last a full trame

bool PPM_state = LOW;
int cur_chan_numb = 0;
unsigned long int trame_elapsed_time = 0;

void handler_led(void) {
  if (led) {
    digitalWrite(BOARD_LED_LIGHT, LOW);
  } else {
    digitalWrite(BOARD_LED_LIGHT, HIGH);
  }
  led = !led;
}

void handle_ppm(void) {
  ppmTimer.pause();

  if(PPM_state) {
    PPM_state = LOW;

    digitalWrite(PPM_out, LOW);

    ppmTimer.setPeriod(LOW_LENGTH);
    trame_elapsed_time += LOW_LENGTH;
  } else {
    PPM_state = HIGH;

    digitalWrite(PPM_out, HIGH);

    if(cur_chan_numb >= NUM_WAYS) {
      ppmTimer.setPeriod(PACKET_LENGTH - trame_elapsed_time);
      cur_chan_numb = 0;
      trame_elapsed_time = 0;
    } else {
      ppmTimer.setPeriod(Channel_data[cur_chan_numb] - LOW_LENGTH);
      cur_chan_numb++;
      trame_elapsed_time += Channel_data[cur_chan_numb];
    }
  }

  ppmTimer.refresh();
  ppmTimer.resume();
}

void setupPPM() {
  ppmTimer.pause();
  // takes microseconds (u)
  ppmTimer.setPeriod(LOW_LENGTH); // 250 ms
  ppmTimer.setMode(0, TIMER_OUTPUT_COMPARE);
  ppmTimer.attachInterrupt(0, handle_ppm);
  ppmTimer.refresh();
  ppmTimer.resume();
}

//
// Revert a channel and store it
void reverse_channel(uint8_t num)
{
	uint16_t val=2048-Channel_data[num];
	if(val>=2048) val=2047;
	Channel_data[num]=val;
}

// Channel value is converted to ppm 860<->2140 -125%<->+125% and 988<->2012 -100%<->+100%
uint16_t convert_channel_ppm(uint8_t num)
{
	uint16_t val=Channel_data[num];
  val = map(val, 0, 4095, 0, 1023);

  // return MIN_PPM + PPM_LENGTH * map16b(val, PPM_MIN_100*2,PPM_MAX_100*2,CHANNEL_MIN_100,CHANNEL_MAX_100);
	// return (((val<<2)+val)>>3)+860;									//value range 860<->2140 -125%<->+125%
}