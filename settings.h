

// Settings are stored here

/*
    Command + Registry + Value + 0x2E ('.')

    ex: s110.
    Set register 1 with value 10

    ex: r1.
    Read register 1

    Command OP Codes are listed below
    OP Code | Description
    S       | Set the settings which gets reflected in realtime
    W       | Write the settings to EEPROM
    R       | Read the settings

    Registry are the settings that can be changed
    Registry | Description
    0x01     | Name of the remote

    Value can be anything and is terminated by 0x2E ('.')

*/

int Settings_changed = 0;

char Settings_Name[10] = {0};
// int Settings_TH_trim = 0;
// int Settings_YW_trim = 0;
// int Settings_PT_trim = 0;
// int Settings_RL_trim = 0;

typedef enum {
  SETTINGS_CMD_UNKNOWN = -1,
  SETTINGS_CMD_SET, // 0
  SETTINGS_CMD_READ, // 1
  SETTINGS_CMD_WRITE, // 2
} Settings_Command;

typedef enum {
  SETTINGS_REG_UNKNOWN = -1,
  SETTINGS_REG_NAME, // 0
  SETTINGS_REG_TH_TRIM,
  SETTINGS_REG_RD_TRIM,
  SETTINGS_REG_EL_TRIM,
  SETTINGS_REG_AI_TRIM,
  SETTINGS_REG_TH_SUBTRIM,
  SETTINGS_REG_RD_SUBTRIM,
  SETTINGS_REG_EL_SUBTRIM,
  SETTINGS_REG_AI_SUBTRIM,
} Settings_Registry;

Settings_Command Settings_Parse_Command(char *input) {
    // value & 0x
    if (input[0] == 's') {
        return SETTINGS_CMD_SET;
    } else if(input[0] == 'r') {
        return SETTINGS_CMD_READ;
    } else if(input[0] == 'w') {
        return SETTINGS_CMD_WRITE;
    } else {
        return SETTINGS_CMD_UNKNOWN;
    }
}

Settings_Registry Settings_Parse_Registry(char *input) {
    char reg = input[1];

    switch(reg) {
        case '0':
        return SETTINGS_REG_NAME;
        break;

        case '1':
        return SETTINGS_REG_TH_TRIM;
        break;

        case '2':
        return SETTINGS_REG_RD_TRIM;
        break;

        case '3':
        return SETTINGS_REG_EL_TRIM;
        break;

        case '4':
        return SETTINGS_REG_AI_TRIM;
        break;

        case '5':
        return SETTINGS_REG_TH_SUBTRIM;
        break;

        case '6':
        return SETTINGS_REG_RD_SUBTRIM;
        break;

        case '7':
        return SETTINGS_REG_EL_SUBTRIM;
        break;

        case '8':
        return SETTINGS_REG_AI_SUBTRIM;
        break;

        default:
        return SETTINGS_REG_UNKNOWN;
    }
}

void * Settings_Parse_Value(char *input) {
    char *rawValue = (input+2);
    // int len = strlen(rawValue);
    char *value2 = rawValue;
    // value2[len] = '\0';
    void *return_value;
    return_value = value2;

    return return_value;
}

void Settings_Do_Command(Settings_Command command, Settings_Registry registry, void *value) {
    if (command == SETTINGS_CMD_SET) {
        switch(registry) {
            case SETTINGS_REG_NAME:
            strcpy(Settings_Name, (char *)value);
            break;

            case SETTINGS_REG_TH_TRIM:
            model.trim[THROTTLE] = atoi((char *)value);
            break;

            case SETTINGS_REG_RD_TRIM:
            model.trim[RUDDER] = atoi((char *)value);
            break;

            case SETTINGS_REG_EL_TRIM:
            model.trim[ELEVATOR] = atoi((char *)value);
            break;

            case SETTINGS_REG_AI_TRIM:
            model.trim[AILERON] = atoi((char *)value);
            break;

            case SETTINGS_REG_TH_SUBTRIM:
            model.subtrim[THROTTLE] = atoi((char *)value);
            break;

            case SETTINGS_REG_RD_SUBTRIM:
            model.subtrim[RUDDER] = atoi((char *)value);
            break;

            case SETTINGS_REG_EL_SUBTRIM:
            model.subtrim[ELEVATOR] = atoi((char *)value);
            break;

            case SETTINGS_REG_AI_SUBTRIM:
            model.subtrim[AILERON] = atoi((char *)value);
            break;
        }

        Settings_changed = 1;
    } else if (command == SETTINGS_CMD_READ) {
        switch(registry) {
            case SETTINGS_REG_NAME:
            // strcpy(Settings_Name, (char *)value);
            break;

            case SETTINGS_REG_TH_TRIM:
            // model.trim[THROTTLE] = atoi((char *)value);
            break;

            case SETTINGS_REG_RD_TRIM:
            // model.trim[RUDDER] = atoi((char *)value);
            break;

            case SETTINGS_REG_EL_TRIM:
            // model.trim[ELEVATOR] = atoi((char *)value);
            break;

            case SETTINGS_REG_AI_TRIM:
            // model.trim[AILERON] = atoi((char *)value);
            break;

            case SETTINGS_REG_TH_SUBTRIM:
            // model.subtrim[THROTTLE] = atoi((char *)value);
            break;

            case SETTINGS_REG_RD_SUBTRIM:
            // model.subtrim[RUDDER] = atoi((char *)value);
            break;

            case SETTINGS_REG_EL_SUBTRIM:
            // model.subtrim[ELEVATOR] = atoi((char *)value);
            break;

            case SETTINGS_REG_AI_SUBTRIM:
            // model.subtrim[AILERON] = atoi((char *)value);
            break;
        }
    } else if (command == SETTINGS_CMD_WRITE) {
        // write to eeprom
    }
}

void __Settings_Read(char *input) {
    Settings_Command command = Settings_Parse_Command(input);
    Settings_Registry registry = Settings_Parse_Registry(input);
    void *value = Settings_Parse_Value(input);

    Serial.print("c: ");
    Serial.print(command);

    Serial.print(" r: ");
    Serial.print(registry);

    Serial.print(" v: ");
    Serial.println(atoi((char*)value));

    Settings_Do_Command(command, registry, value);
}

void Settings_Read() {
    char buffer[30] = {0};

    Serial.setTimeout(1000);
    Serial.readBytesUntil('.', buffer, 30);
    Serial.flush();

    __Settings_Read(buffer);
}
