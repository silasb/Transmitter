# Transmitter

Building a [Devo7e] from scratch, or something similar.

Using a [LeafLabs Maple r5] running [Arduino_STM32]

NRF24L01 talking Bayang via a [nrf24-multipro]

# Compile

Install two libraries: `Adafruit_SSD1306` and `Adafruit-GFX-Library`.
See [Modifications](#modifications) section for modifications that we need to make to the `Adafruit_SSD1306` library in order to get it setup for the screen we are using.

Install `Arduino_STM32`. If you are on windows make sure you install the drivers or Arduino won't be able to find the device after it enters DFU mode.

Select `Maple (Rev 3)` from the boards list.

Select the correct serial port.

Then click upload.

## Pins on [LeafLabs Maple r5]

	D6 PA8 PPM_out

Using `D15—D20` for Low-Noise ADC pins to read data.

	D15 PC0 CH10 rv_stk
	D16 PC1 CH11 lv_stk
	D17 PC2 CH12 lh_stk
	D18 PC3 CH13 rh_stk
	; D19 PC4 CH14 battery voltage

## Servo theory

	1ms far right
	1.5ms middle
	2ms far left

## Modifications

Need to modify `Adafruit_SSD1306.h` to:

	#define SSD1306_128_64
	// #define SSD1306_128_32

[TeensyPPM]: https://forum.pjrc.com/threads/25890-New-Pulse-Position-Modulation-%28PPM%29-Library
[PulsePosition Library]: https://www.pjrc.com/teensy/td_libs_PulsePosition.html
[diy-remote]: https://github.com/wertarbyte/funkenschlag/
[nrf24-multipro]: https://github.com/goebish/nrf24_multipro
[DIY-Multiprotocol-TX-Module]: https://github.com/pascallanger/DIY-Multiprotocol-TX-Module
[reading_ppm_via_mcu]: http://www.avrfreaks.net/forum/read-ppm-signal-rc-module
[Bayang_protocol]: https://github.com/silver13/H8mini_blue_board/blob/master/Silverware/src/rx_bayang_protocol.c
[LeafLabs Maple r5]: http://docs.leaflabs.com/docs.leaflabs.com/index.html
[Devo7e]: http://www.deviationtx.com/wiki/hardware/devo7e
[Arduino_STM32]: http://wiki.stm32duino.com/index.php?title=Main_Page